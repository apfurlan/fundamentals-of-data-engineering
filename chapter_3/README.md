# Designing Good Data Architecture


São consideradas três referencias : TOGAF's, Gartner e EABOK

TOGAF's
TOGAF é o Open Group Architecture Framework, um padrão do The Open Group. 
É apontado como o framework de arquitetura mais usado atualmente. Ele
pode ser definido da seguinte forma : 

> *O termo “empresa” no contexto de “arquitetura corporativa” pode denotar
 uma empresa inteira – abrangendo todos os seus serviços de informação e 
tecnologia, processos e infraestrutura – ou um domínio específico dentro 
da empresa. Em ambos os casos, a arquitetura cruza vários sistemas e 
vários grupos funcionais dentro da empresa*.

Gartner é uma empresa global de pesquisa e consultoria que produz  artigos 
de pesquisa e relatórios sobre tendências relacionadas a empreendimentos. 
A  definição do Gartner é a seguinte:

> *A arquitetura corporativa (EA) é uma disciplina para liderar de 
forma proativa e holística respostas corporativas a forças 
disruptivas, identificando e analisando a execução de mudança em 
direção à visão de negócios e resultados
desejados. A EA agrega valor apresentando aos líderes de negócios 
e de TI recomendações prontas para assinatura para ajustar políticas
e projetos para atingir resultados de negócios direcionados que 
capitalizam em negócios relevantes
interrupções*.

EABOK - é que livro produzido em 2004 e ainda que parece antigo ele 
é frequentemente referenciado para descrever entreprise archtecture. 
Aqui está a definição do EABOK:

> *_Enterprise Architecture (EA) é um modelo organizacional; uma 
representação abstrata de uma empresa que alinha estratégia, operações 
e tecnologia para criar um roteiro para sucesso.*

segundo os autores : 
> *A arquitetura empresarial é o projeto de sistemas para dar 
suporte à _mudança na empresa, alcançada por decisões flexíveis e 
reversíveis alcançadas por meio de  avaliação cuidadosa de
compensações.*

Adotar uma cultura de decisões reversíveis ajuda a superar essa tendência 
ao reduzindo o risco associado a uma decisão. (porta de mão unica e mão
dupla) Por outro lado, uma porta de mão dupla é uma decisão facilmente 
reversível: você entra e prossiga se gostar do que vê na sala ou volte 
pela porta se você não

Os arquitetos não estão simplesmente mapeando processos de TI e olhando 
vagamente para um futuro distante e utópico; eles resolvem ativamente 
problemas de negócios e criam novas oportunidades tunidades. 
**As soluções técnicas não existem por si mesmas, mas para apoiar os negócios**. 
Os arquitetos identificam problemas no estado atual (baixa qualidade 
de dados, escalabilidade limites, linhas de negócios com perda de dinheiro), 
define estados futuros desejados (ágil qualidade de dados melhoria, 
soluções de dados em nuvem escaláveis, processos de negócios aprimorados) 
e realizar iniciativas por meio da execução de pequenas etapas concretas.

## Boa arquitetura de dados

“boa” arquitetura de dados significa bons requisitos de negócios de 
arquitetura de dados com um conjunto comum e amplamente
reutilizável de blocos de construção, mantendo a flexibilidade e
fazer as devidas compensações.

Uma boa arquitetura de dados é flexível e de fácil manutenção. Ele 
evolui em resposta às mudanças dentro do negócio A arquitetura de 
dados ruim é fortemente acoplada, rígida, excessivamente centralizada

As correntes ocultas do ciclo de vida da engenharia de dados formam a base 
de uma boa arquitetura de dados para empresas em qualquer estágio de maturidade 
de dados (segurança, gerenciamento de dados, DataOps, arquitetura de dados, 
orquestração e Engenharia de software).

## Principios de uma boa arquitetura de dados
AWS Well-Architected Framework e Five Principles for 
Cloud-Native Architecture da google cloud.


### Esoclha componentes sabiamente 
Componentes comuns podem ser qualquer coisa que tenha ampla aplicabilidade 
dentro de uma organização. Por exemplo, a separação de computação e 
armazenamento em sistemas de dados em nuvem permite que os usuários 
acessem um camada de armazenamento

### Plan for Failure
Para construir sistemas de dados altamente robustos, você 
deve considerar falhas em seus projetos. 

Disponibilidade, Confiabilidade, Objetivo do tempo de recuperação, Recovery point objective - Nesta configuração, 
o objetivo do ponto de recuperação (RPO) refere-se ao 
máximo perda de dados aceitável.

### Architect for Scalability
Um sistema elástico pode escalar dinamicamente em resposta 
à carga, idealmente em uma moda automatizada. Quando for
possível, cogite usar ferramentas serverless. Meça ! 
Meça sua carga atual, picos de carga aproximados e estime 
a carga nos próximos vários anos para determinar se sua 
arquitetura de banco de dados é apropriada.


### Architecture Is Leadership
Os arquitetos de dados devem ser altamente competentes
tecnicamente, mas delegar a maioria dos colaborador 
trabalha para os outros. Fortes habilidades de liderança
combinadas com alto nível técnico competência são raras 
 e extremamente  valiosas. É orientar o equipe de
 desenvolvimento, para elevar seu nível para que possam 
 assumir questões mais complexas.


Eles possuem a técnica habilidades de um engenheiro de 
dados, mas não pratica mais a engenharia de dados no dia 
a dia; eles oriente os engenheiros de dados atuais, 
faça escolhas tecnológicas cuidadosas em consulta com sua organização e disseminar expertise por meio de treinamento 
e liderança.


### Principle 5: Always Be Architecting
Uma arquitetura não é estática. O arquiteto de dados mantém 
uma arquitetura de destino e planos de sequenciamento que 
mudam ao longo do tempo. A arquitetura de destino torna-se 
um movimento 
alvo, ajustado em resposta às mudanças de negócios e tecnologia interna e externamente.

### Princípio 6: Construir Sistemas Fracamente Acoplados
O advento do API Mandate de Bezos é amplamente visto como um  divisor de águas para _Amazon_. 
Colocar dados e serviços atrás de APIs permitiu o baixo acoplamento e acabou resultando na AWS 
como a conhecemos agora. Um sistema fracamente acoplado tem as seguintes propriedades:
1. Os sistemas são divididos em vários componentes pequenos.
2. Esses sistemas interagem com outros serviços por meio de camadas de abstração, através de 
mensageria ou API.

O baixo acoplamento de tecnologia e sistemas humanos permitirá que 
seu engenheiro de dados equipes para colaborar de forma mais 
eficiente uns com os outros e com outras partes do empresa.


### Principle 7: Make Reversible Decisions
Você deve buscar decisões reversíveis, pois eles tendem a simplificar sua arquitetura e mantê-la ágil.
“portas de mão dupla” - (decisões reversíveis)

### Principle 8: Prioritize Security
As arquiteturas tradicionais colocam muita fé na segurança do
 perímetro,  um  perímetro de rede aprimorado com “coisas
 confiáveis” dentro e “coisas não confiáveis” fora. Infelizmente, essa abordagem sempre foi vulnerável a ataques internos, bem 
 como a ameaças externas, como spear phishing.

em um ambiente de cloud este limite é ainda mais dramático. 
Todos os recursos estao ligados a internet em algum nível.

O modelo de responsabilidade compartilhada, a _Amazon_ enfatiza o
modelo de responsabilidade compartilhada, que divide a segurança
em segurança da nuvem 
e segurança na nuvem.

Ainda assim, é em última análise, a responsabilidade do usuário de projetar 
um modelo de segurança para seus aplicativos e dados e aproveitar os recursos 
da nuvem para concretizar esse modelo. Aqueles que lidam com dados devem 
presumir que são em última instância, responsável por protegê-lo

### Princípio 9: Adote FinOps

FinOps é uma disciplina de gerenciamento financeiro em nuvem em evolução e 
uma prática cultural que permite que as organizações obtenham o máximo valor 
comercial ajudando engenharia, finanças, tecnologia e equipes de negócios para 
colaborar em decisões de gastos baseadas em dados.

Overbuying implica dinheiro desperdiçado, enquanto subcompra significa dificultando 
futuros projetos de dados e conduzindo um tempo significativo de pessoal para 
controlar carga do sistema e tamanho dos dados; a subcompra pode exigir ciclos de 
atualização de tecnologia mais rápidos, com custos adicionais associados.

Agora é possível escalar para alto desempenho, e, em seguida, reduza para economizar 
dinheiro. No entanto, a abordagem de pagamento conforme o uso torna gastando muito 
mais dinâmico.

## Major Architecture Concepts

Em meio a esse turbilhão de atividades, não deve perder de vista o 
objetivo principal de todas essas arquiteturas: Levar dados e transformá-lo em algo útil para consumo.

Um serviço é um conjunto de funcionalidades cujo objetivo é realizar uma
tarefa. (processamento de pedidos de vendas)

Um domínio pode conter vários serviços. Por exemplo, você pode ter um 
domínio de vendas com três serviços: pedidos, faturamento e produtos. Cada serviço tem tarefas específicas
Ao pensar sobre o que constitui um domínio, concentre-se no que o domínio representa no 
mundo real e trabalhe de trás para frente.

## Distributed Systems, Scalability, and Designing for Failure

Escalabilidade. Elasticidade, Disponibilidade, Confiabilidade

Máquinas individuais geralmente não podem oferecer alta disponibilidade 
e confiabilidade. Usamos um sistema distribuído para obter maior capacidade 
de dimensionamento geral e maior disponibilidade e confiabilidade. O 
dimensionamento horizontal permite adicionar mais máquinas para atender 
aos requisitos de carga e recursos

Os dados são replicados para que, se uma máquina morrer, as outras 
máquinas pode continuar de onde o servidor ausente parou; o cluster pode 
adicionar mais máquinas para restaurar a capacidade.


## Tight Versus Loose Coupling: Tiers, Monoliths, and Microservices
Cada parte de um domínio e serviço é vitalmente dependente de todos 
os outros domínios e serviços. Esse padrão é conhecido como fortemente 
acoplado. No outro extremo do espectro, você tem domínios e serviços 
descentralizados que não dependem estritamente uns dos outros, em um 
padrão conhecido como acoplamento flexível. Projetar uma “boa” 
arquitetura de dados depende de compensações entre o
e baixo acoplamento de domínios e serviços.

**Single tier** Em uma arquitetura de camada única, seu banco de dados 
e seu aplicativo estão fortemente acoplados, residindo em um único servidor 
(Figura 3-5). Mesmo quando arquiteturas de camada única construídas em 
redundância (por exemplo, uma réplica de failover), elas apresentam 
limitações significativas de outras maneiras. Por exemplo, muitas vezes não 
é prático (e não é aconselhável) executar consultas analíticas em bancos 
de dados de aplicativos de produção. Fazer isso corre o risco de sobrecarregar 
o banco de dados e fazer com que o aplicativo para ficar indisponível.

**Multitier** . Uma arquitetura multicamadas (também conhecida como n-camadas) 
é composta de camadas separadas: dados, aplicação, lógica de negócios, 
apresentação, etc. Cada camada é isolada da outra, permitindo a separação de
preocupações. Com uma arquitetura de três camadas, você pode usar qualquer 
tecnologia de sua preferência em cada camada, sem a necessidade de focar 
monoliticamente.

**Monoliths** Em sua versão mais extrema, um monólito consiste em uma 
única base de código executada em uma única máquina que fornece a 
lógica do aplicativo e a interface do usuário. O acoplamento dentro de 
monólitos pode ser visto de duas maneiras: acoplamento técnico e 
acoplamento de domínio. Você pode ter um aplicativo com várias camadas 
desacopladas em uma arquitetura multicamadas, mas ainda compartilhar vários 
domínios. Ou você pode ter uma arquitetura de camada única atendendo a um único domínio.

O acoplamento apertado de um monólito implica na falta de modularidade de seus componentes. Trocar ou atualizar componentes em um monólito geralmente 
é um  exercício de troca de uma dor por outra. Devido à natureza fortemente acoplada, 
a reutilização de componentes em toda a arquitetura é difícil ou impossível.

**Micro-serviços** A arquitetura de microsserviços compreende 
serviços separados, descentralizados e fracamente acoplados. Cada serviço tem 
uma função específica e é dissociado de outros serviços que operam em seu 
domínio. Se um serviço ficar temporariamente inativo, isso não afetará a 
capacidade de outros serviços continuarem funcionando.

### Considerations for data architecture
O data warehouse central é inerentemente monolítico. Um movimento em 
direção a um equivalente de microsserviços com um data warehouse é desacoplar
o fluxo de trabalho com pipelines de dados específicos de domínio conectando-se a data warehouses específicos de domínio correspondentes. Por exemplo, o pipeline de dados de vendas se conecta ao específico de vendas
data warehouse e os domínios de inventário e produto seguem um padrão semelhante.
Nosso conselho: os monólitos não são necessariamente ruins e pode fazer sentido começar com
um sob certas condições. Às vezes, você precisa se mover rapidamente e é muito mais simples começar com um monólito. Apenas esteja preparado para quebrá-lo em pedaços menores eventualmente; não fique muito confortável.

## User Access: Single Versus Multitenant

Temos dois fatores a considerar na multilocação: desempenho e segurança.
em relação à segurança, os dados de diferentes locatários devem ser 
devidamente isolados. Quando uma empresa tem vários inquilinos de clientes externos, 
esses inquilinos não devem estar cientes uns dos outros, e os engenheiros devem evitar 
o vazamento de dados.

## Event-Driven Architecture
Um fluxo de trabalho orientado a eventos (Figura 3-8) abrange a capacidade de 
criar, atualizar e mover eventos de forma assíncrona em várias partes do ciclo 
de vida da engenharia de dados. Esse fluxo de trabalho se resume a três áreas 
principais: *produção de eventos*, *roteamento* e *consumo* como mostra a figura 
abaixo.

![""](./img/event_driven_arch.png)

Um evento deve ser produzido e encaminhado para algo que o consuma sem 
dependências fortemente acopladas entre o produtor, o roteador do evento 
e o consumidor.

## Brownfield Versus Greenfield Projects

### Brownfield 
Projetos brownfield requerem um entendimento completo da arquitetura legada e a interação de várias tecnologias antigas e novas. Com muita frequência, é fácil criticar o trabalho e as decisões de uma equipe anterior, mas é muito melhor cavar fundo,
fazer perguntas e entender por que as decisões foram tomadas. Uma alternativa popular para uma reescrita direta é o padrão estrangulador: novos sistemas substituem lenta e gradualmente os componentes de uma arquitetura legada. Isso permite decisões flexíveis e reversíveis ao avaliar o impacto da descontinuação em sistemas dependentes.

### Greenfiled
um projeto greenfield permite que você seja pioneiro em um novo começo, sem restrições pela história ou legado de uma arquitetura anterior. Projetos greenfield tendem a ser mais fáceis do que projetos brownfield, e muitos arquitetos de dados e
os engenheiros os acham mais divertidos!


## Examples and Types of Data Architecture

### Data Warehouse
Um data warehouse é um hub de dados central usado para relatórios e análises. Os dados em um data warehouse geralmente são altamente formatados e estruturados para casos de uso de análise. Isso era caro e trabalhoso. Desde então, o modelo escalável de pagamento conforme o uso tornou a nuvem
armazéns de dados acessíveis até mesmo para pequenas empresas. Porque um provedor terceirizado
gerencia a infraestrutura de data warehouse, as empresas podem fazer muito mais com menos
pessoas, mesmo com o aumento da complexidade de seus dados.




