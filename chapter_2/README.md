
# The Data Engineering Lifecycle

Devido ao aumento da abstração técnica, os engenheiros de dados
tornaram-se cada vez mais engenheiros do ciclo de vida dos dados.  
Ou seja Engenheiros de dados são responsáveis por toda a gestão
do ciclo de vida dos dados. 



## O que é *Data Engineering Lifecycle*

O **ciclo de vida da engenharia** de dados compreende estágios que 
transformam ingredientes de dados brutos em um produto final útil, 
pronto para consumo por usuários finais como analistas, cientistas 
de dados, engenheiros de ML e outros. Os estagios do ciclo de vida 
da engenharia de dados
    - Geração
    - Armazenamento
    - Ingestão
    - Transformacão
    - Dados de serviço

Dentro todas as etapas mencionadas acima, o armazento acorre durante
todo o ciclo de vida do dados. A cada etapa de transformação do dado
utlizamos storage.


obtendo dados dos sistemas de origem, em seguinda para nosso objetivo central, 
fornecer dados para os times de AD, CD e ML. o armazenamento ocorre durante 
todo o ciclo de vida à medida que os dados fluem do começo ao fim – portanto, 
o diagrama mostra o “estágio” de armazenamento como uma base que sustenta 
outros estágios.

![""](./img/data_eng_lifecycle.png)

Vários estágios do ciclo de vida podem se repetir, ocorrer fora de ordem, 
se sobrepor ou se entrelaçar de forma interessante e maneiras inesperadas. 
Atuando como um alicerce estão as subcorrentes (Figura 2-1, abaixo) que 
atravessam vários estágios do ciclo de vida da engenharia de dados: 
segurança, gerenciamento de dados, DataOps, data arquitetura, orquestração 
e engenharia de software. Nenhuma parte da engenharia de dados ciclo de 
vida pode funcionar adequadamente sem essas subcorrentes.


## The Data Lifecycle Versus the Data Engineering Lifecycle

O ciclo de vida da engenharia de dados é um subconjunto de todo o 
ciclo de vida dos dados. Ciclo de vida se concentra nos estágios que um 
engenheiro de dados controla.


## Generation: Source Systems


sistema de origem é a origem dos dados,pode ser um dispositivo IoT, uma 
fila de mensagens de aplicativo ou um banco de dados transacional. 
O engenheiro de dados precisa ter uma compreensão prática de como os sistemas 
de origem funcionam, a maneira como eles geram dados, a frequência e a 
velocidade  dos dados e a variedade de dados que eles geram.

Um grande desafio na engenharia de dados é a variedade estonteante de 
sistemas de fonte de dados os engenheiros devem trabalhar e compreender

Este padrão(imagem) de sistema de origem tornou-se popular na década de 1980
com o sucesso explosivo dos sistemas de gerenciamento de banco de dados relacional 
(RDBMSs). O padrão de aplicativo + banco de dados permanece popular hoje em dia 
com várias evoluções modernas.

![""](./img/pattern_deprecated.png)

A Figura 2-4 ilustra um enxame de IoT:
uma frota de dispositivos (círculos) envia mensagens de dados (retângulos) 
para uma coleção central sistema. Este sistema de origem IoT é cada vez mais 
comum como dispositivos IoT, como sensores, dispositivos inteligentes e muito 
mais aumentam na natureza.

![""](./img/iot.png)

## Evaluating source systems: Key engineering considerations


Há muitas coisas a considerar ao avaliar os sistemas de origem, no 
entanto existem questões que todo ED deve avaliar, são algumas :

    - Como os dados são mantidos no sistema de origem? Os dados persistem a longo 
    prazo ou são excluído temporariamente e rapidamente?
    
    - Em que taxa os dados são gerados? Quantos eventos por segundo? quantos giga-
    bytes por hora?    
    
    - Qual é o *schema* dos dados ingeridos? Os engenheiros de dados precisarão 
    se juntar em várias tabelas ou mesmo vários sistemas para obter uma visão 
    completa dos dados?
    
    • Se o esquema mudar (digamos, uma nova coluna for adicionada), como 
    isso é tratado e comunicados às partes interessadas ?

    • A leitura de uma fonte de dados afetará seu desempenho?

OBS : O livro lista muito mais questões importantes, aqui destaquei aquelas
que já presenciei. 


Um engenheiro de dados deve saber como a fonte gera dados, incluindo 
peculiaridades e limites de cada fonte. Um item importante da origem 
de dados é o *schema*. Em relação à schema, as fontes podem ter seu
schema declarado previamente (relacional) ou já ter seu *schema* 
declarado no momento da gravação do dado (não-relacional). 
**Uma parte fundamental do trabalho do engenheiro de dados é obter a entrada de 
dados brutos no esquema do sistema de origem e transformando-o em uma saída 
valiosa para análises**.

## Storage
Você precisa de um lugar para armazenar dados e escolher como fazer isso
é uma tarefa bem complexa e em geral mais de uma solução é usada nesta
etapa. Algumas soluções apresentam ainda recursos de consulta (S3). 

O armazenamento é uma etapa que está presente em todo o ciclo de vida
da engenharia de dados (veja figura novamente). **Além disso a forma
como armazenamos os dados dirão sobre como utilizaremos ele**. 
(DW x Streaming).


## Evaluating storage systems: Key engineering considerations

Questões a serem fieitas antes de escolher um sistema de 
armazenamento.

    - compatível com os requisitos de gravação e leitura da arquitetura? velocidades?
    - Este sistema de armazenamento lidará com a escala futura prevista?
    - Os usuários e processos downstream serão capazes de recuperar dados no necessário contrato de nível de serviço (SLA)?
    - Você está capturando metadados sobre a evolução do esquema, fluxos de dados, linhagem de dados, e assim por diante? Os metadados têm um impacto significativo na utilidade dos dados. Metaos dados representam um investimento no futuro, melhorando drasticamente a descoberta e o conhecimento institucional para otimizar projetos e arquitetura futuros
    mudanças.
    - O sistema de armazenamento é independente de esquema (armazenamento de objetos)? Esquema flexível (Cassan‐
    Dra)? Esquema imposto (um data warehouse na nuvem)?



## Understanding data access frequency

Isso traz à tona a noção de “temperaturas” de dados. A frequência 
de acesso aos dados. Os dados acessados com mais/menos frequência 
são chamados de dados quentes/frios. Dados quentes são comumente
acessados muitas vezes por dia, talvez até várias vezes por segundo.
Esses dados devem ser armazanados para consulta rápida. 

Dados frios são mantidos para recuperação de desastres ou 
conformidade. Hoje os provedores de cloud fornecem tipos
 específicos de armazenamento para estes dados como é o 
 caso do S3 Glacier e o Azure Archive. 

A melhor solução de armazenamento é aquele que melhor se 
adequa ao seu caso de uso. 

## Ingestion

A ingestão de dados representam os gargalos mais significativos
do cliclo de vida da engenharia do dado. Dentre as diversas 
cuasas, o  funcionamento das fontes de dados podem responder 
de maneira inesperada e isso não está em nosso controle. 
Decorre disso a interrupção total/parcial do fornecimento 
de dado.

## Key engineering considerations for the ingestion phase

 - Qual é o destino dos dados após a ingestão?
 - Com que frequência precisarei acessar os dados?
 - Em que formato estão os dados? Meu armazenamento e 
 transformação downstream podem os sistemas lidam com esse 
 formato?


## Batch versus streaming

A ingestão em lote é simplesmente um processo especializado e
maneira conveniente de processar esse fluxo em grandes partes - 
por exemplo, manipular umdados de um dia inteiro em um único lote.

A ingestão de streaming nos permite fornecer dados para sistemas 
downstream - seja outros aplicativos, bancos de dados ou sistemas analíticos 
— de forma contínua e em tempo real. Aqui, tempo real (ou quase em tempo real) 
significa que os dados estão disponíveis para um sistema a jusante pouco tempo 
depois de serem produzidos (por exemplo, menos de um segundo depois).

No entanto, a separação de armazenamento e computação em muitos sistemas 
e a onipresença
de streaming de eventos e plataformas de processamento tornam o 
processamento contínuo de dados
fluxos muito mais acessíveis e cada vez mais populares. A escolha 
depende muito
sobre o caso de uso e as expectativas de pontualidade dos dados.

## Key considerations for batch versus stream ingestion

• Preciso de ingestão de dados em milissegundos em tempo real? Ou seria um micro-lote
abordagem do trabalho, acumulando e ingerindo dados, digamos, a cada minuto?

Meu pipeline de streaming e sistema são confiáveis e redundantes se a infraestrutura
falha?

Quais ferramentas são mais apropriadas para o caso de uso? Devo usar um serviço 
gerenciado (Amazon Kinesis, Google Cloud Pub/Sub, Google Cloud Dataflow) ou 
stand up minhas próprias instâncias de Kafka, Flink, Spark, Pulsar, etc.? Se 
eu fizer o último, quem vai gerenciá-lo? Quais são os custos e compensações?


Adote o verdadeiro streaming em tempo real somente depois de identificar 
um uso comercial caso que justifique as compensações contra o uso de batch. 
**Adote novas ferramentas somente quando elas apresentarem uma clara vantagem**

## *Push* versus *Pull*

    -   Modelo push : sistema de origem grava dados em um destino
    -   Modelo pull : dados são recuperados do sistema de origem

Defini todos o processo de ingestão como *push* e ou *pull* pode 
ser bastante complexo uma vez que  diversas etapas intermediárias
de push e pull de dados acontecem ao longo de um pipeline de dados.


A parte extração (E) do ETL esclarece que estamos lidando
com um modelo de ingestão *pull*. Com a ingestão de streaming, os dados 
ignoram um banco de dados de back-end e são enviados diretamente para um 
endpoint. Esse O padrão é útil com frotas de sensores IoT que emitem dados 
do sensor.


## Transformação 

A próxima etapa do ciclo de vida da engenharia de dados é a transformação, 
o que significa que os dados precisam ser alterado de sua forma original para 
algo útil para casos de uso downstream. Essa alterações consistem de maperm 
os dados nos tipos corretos, colocar registros em formatos padrão, 
removendo os ruins (nulls) até transformar o esquema de dados e aplicar a 
normalização

### Considerações chaves para a fase de transformacão 

Alguma considerações a serem feitas : 

• Qual é o custo e o retorno do investimento (ROI) da transformação? O que é
o valor comercial associado?

• Quais regras de negócios são suportadas pelas transformações?

de transformações de streaming para continuar crescendo, talvez substituindo 
totalmente o batch
processamento em certos domínios em breve.

Normalmente, os dados são
transformado em sistemas de origem ou em voo durante a ingestão. Por exemplo, uma fonte
o sistema pode adicionar um timestamp de evento, pode ser “enriquecido” com 
informações adicionais campos e cálculos antes de serem enviados para um data warehouse.


**Preparação de dados, manipulação de dados e
limpeza — essas tarefas transformadoras agregam valor para 
os consumidores finais de dados**. A lógica de negócios é um dos 
principais impulsionadores da transformação de dados,

A caracterização de dados para ML é outro processo de transformação 
de dados. Destaque
pretende extrair e aprimorar recursos de dados úteis para treinar modelos 
de ML. A featurização pode ser uma arte obscura, combinando experiência de 
domínio (para identificar quais recursos
pode ser importante para previsão) com ampla experiência em ciência de dados.

A caracterização de dados para ML é outro processo de transformação de dados. Destaque
pretende extrair e aprimorar recursos de dados úteis para treinar modelos de ML. A featurização pode ser uma arte obscura, combinando experiência de domínio (para identificar quais recursos
pode ser importante para previsão) com ampla experiência em ciência de dados.


## Serving data 

Servir os dados é a ultima etapa do ciclo de vida da engenharia de 
dados. Neste ponto os dados já estão ingeridos e limpos e então
podemos obter valor dos dados. Esta parte pode ser considerada
a mais empolgante do ciclo de vida da ED, é neste momento que os
 dados dão origem à informação e engenheiros de ML pode aplicar
técnicas mais apropriadas ao ML. 

### Analytics

Analytics is the core of most data endeavors. Once your data is stored and trans‐
formed, you’re ready to generate reports or dashboards and do ad hoc analysis on the
data. Whereas the bulk of analytics used to encompass BI, it now includes other facets

**Inteligência de negócios** ,

Análise operacional : Os tipos de
insights em análise operacional diferem do BI tradicional, pois a análise operacional
é focado no presente e não necessariamente diz respeito a tendências históricas.

Embedded analytics : (análise voltada para o cliente) : Com análises incorporadas, a taxa de solicitação de relatórios e a burocracia correspondente
den em sistemas analíticos, sobe dramaticamente; controle de acesso é significativamente mais
complicado e crítico. As empresas podem estar fornecendo análises e dados separados para
milhares ou mais clientes.

**Machine Learning** - Quando as organizações atingem um alto nível de maturidade de dados, elas podem começar a identificar
problemas passíveis de ML e começar a organizar uma prática em torno dele.

os limites entre engenharia de dados, engenharia de ML e engenharia analítica
pode ser confuso.

Na prática, os engenheiros de dados fazem parte da equipe principal de suporte para recursos
lojas para dar suporte à engenharia de ML.